<?php

namespace ReservasBundle\Twig;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DateExtension
 *
 * @author Jota
 */
class DateExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
            new \Twig_SimpleFilter('timeAgo', array($this, 'timeAgo')),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',') {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$' . $price;

        return $price;
    }

    public function timeAgo($date) {
        $time = time() - strtotime($date);

        $units = array(
            31536000 => 'año',
            2592000 => 'mes',
            604800 => 'semana',
            86400 => 'día',
            3600 => 'hora',
            60 => 'minuto',
            1 => 'segundo'
        );

        foreach ($units as $unit => $val) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return ($val == 'segundos') ? 'hace unos segundos' :
                    (($numberOfUnits > 1) ? "hace $numberOfUnits" : 'hace')
                    . ' ' . $val . (($numberOfUnits > 1) ? 's' : '');
        }
    }

    public function getName() {
        return 'app_extension';
    }

}
