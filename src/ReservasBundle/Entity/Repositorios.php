<?php

namespace ReservasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Repositorios
 * @ORM\Entity(repositoryClass="ReservasBundle\Entity\Repository\RepositoriosRepository")
 * @ORM\Table(name="repositorios")
 */
class Repositorios
{
    /**
     * @var int
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "integer",
     *     nullable = false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;

    /**
     * @ORM\Column(name="url", type="string")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="Cabeceras", inversedBy="repositorios")
     * @ORM\JoinColumn(name="cabecera_id", referencedColumnName="id")
     */
    private $cabecera;

    /**
     * @ORM\OneToMany(targetEntity="Reservas", mappedBy="repositorio")
     */
    private $reservas;

    private $ocupado = false;

    public function __toString() {
        $nombreCabecera = $this->cabecera->getNombre();
        return "$nombreCabecera - $this->nombre";
    }

    public function getNombreCompleto() {
        $cabecera = $this->cabecera;
        $nombreCabecera = $cabecera->getNombre();
        $nombreCabecera = str_replace(' ', '', $nombreCabecera);
        return "$nombreCabecera/$this->nombre";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Repositorios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Repositorios
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set cabecera
     *
     * @param \ReservasBundle\Entity\Cabeceras $cabecera
     * @return Repositorios
     */
    public function setCabecera(\ReservasBundle\Entity\Cabeceras $cabecera = null)
    {
        $this->cabecera = $cabecera;

        return $this;
    }

    /**
     * Get cabecera
     *
     * @return \ReservasBundle\Entity\Cabeceras
     */
    public function getCabecera()
    {
        return $this->cabecera;
    }

    /**
     * Add reservas
     *
     * @param \ReservasBundle\Entity\Reservas $reservas
     * @return Repositorios
     */
    public function addReserva(\ReservasBundle\Entity\Reservas $reservas)
    {
        $this->reservas[] = $reservas;

        return $this;
    }

    /**
     * Remove reservas
     *
     * @param \ReservasBundle\Entity\Reservas $reservas
     */
    public function removeReserva(\ReservasBundle\Entity\Reservas $reservas)
    {
        $this->reservas->removeElement($reservas);
    }

    /**
     * Get reservas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas()
    {
        return $this->reservas;
    }

    /**
     * Get ocupado
     *
     * @return boolean
     */
    public function getOcupado()
    {
        foreach ($this->reservas as $key => $reserva) {
            if($reserva->isActive())
                return true;
        }
        return false;
    }
}
