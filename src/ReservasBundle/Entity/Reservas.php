<?php

namespace ReservasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservas
 * @ORM\Entity(repositoryClass="ReservasBundle\Entity\Repository\ReservasRepository")
 * @ORM\Table(name="reservas")
 */
class Reservas
{
    /**
     * @var int
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "integer",
     *     nullable = false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="fechainicio", type="datetime")
     */
    private $fechainicio;

    /**
     * @ORM\Column(name="fechafin", type="datetime")
     */
    private $fechafin;

    /**
     * @ORM\Column(name="responsable", type="string")
     */
    private $responsable;

    /**
     * @ORM\Column(name="descripcion", type="string")
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Repositorios", inversedBy="reservas")
     * @ORM\JoinColumn(name="reserva_id", referencedColumnName="id")
     */
    private $repositorio;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechainicio
     *
     * @param \DateTime $fechainicio
     * @return Reservas
     */
    public function setFechainicio($fechainicio)
    {
        $this->fechainicio = $fechainicio;

        return $this;
    }

    /**
     * Get fechainicio
     *
     * @return \DateTime
     */
    public function getFechainicio()
    {
        return $this->fechainicio;
    }

    /**
     * Set fechafin
     *
     * @param \DateTime $fechafin
     * @return Reservas
     */
    public function setFechafin($fechafin)
    {
        $this->fechafin = $fechafin;

        return $this;
    }

    /**
     * Get fechafin
     *
     * @return \DateTime
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return Reservas
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Reservas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set repositorio
     *
     * @param \ReservasBundle\Entity\Repositorios $repositorio
     * @return Reservas
     */
    public function setRepositorio(\ReservasBundle\Entity\Repositorios $repositorio = null)
    {
        $this->repositorio = $repositorio;

        return $this;
    }

    /**
     * Get repositorio
     *
     * @return \ReservasBundle\Entity\Repositorios
     */
    public function getRepositorio()
    {
        return $this->repositorio;
    }

    public function isActive(){
        $now = new \DateTime();
        return $this->fechainicio < $now && $this->fechafin > $now ;
    }
}
