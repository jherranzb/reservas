<?php

namespace ReservasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proyectos
 * @ORM\Entity(repositoryClass="ReservasBundle\Entity\Repository\ProyectosRepository")
 * @ORM\Table(name="proyectos")
 */
class Proyectos
{
    /**
     * @var int
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "integer",
     *     nullable = false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** 
     * @ORM\Column(name="nombre", type="string") 
     */
    private $nombre;

    /** 
     * @ORM\Column(name="fechainicio", type="datetime") 
     */
    private $fechainicio;

    /** 
     * @ORM\Column(name="fechafin", type="datetime") 
     */
    private $fechafin;

    /** 
     * @ORM\Column(name="descripcion", type="string") 
     */
    private $descripcion;

    /** 
     * @ORM\Column(name="product", type="string") 
     */
    private $product;

    /** 
     * @ORM\Column(name="project", type="string") 
     */
    private $project;

    /** 
     * @ORM\Column(name="scrum", type="string") 
     */
    private $scrum;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cabeceras", inversedBy="proyectos")
     * @ORM\JoinColumn(name="proyecto_id", referencedColumnName="id")
     */
    private $cabecera;
}
