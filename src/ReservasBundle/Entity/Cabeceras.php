<?php

namespace ReservasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cabeceras
 * @ORM\Entity(repositoryClass="ReservasBundle\Entity\Repository\CabecerasRepository")
 * @ORM\Table(name="cabeceras")
 */
class Cabeceras
{
    /**
     * @var int
     *
     * @ORM\Column(
     *     name = "id",
     *     type = "integer",
     *     nullable = false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="nombre", type="string")
     */
    protected $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Repositorios", mappedBy="cabecera")
     */
    protected $repositorios;

    /**
     * @ORM\OneToMany(targetEntity="Proyectos", mappedBy="cabecera")
     */
    protected $proyectos;

    protected $ocupado = false;

    /**
     * @ORM\Column(name="color", type="string")
     */
    protected $color;

    public function __toString() {
        return $this->nombre;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repositorios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proyectos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cabeceras
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add repositorios
     *
     * @param \ReservasBundle\Entity\Repositorios $repositorios
     * @return Cabeceras
     */
    public function addRepositorio(\ReservasBundle\Entity\Repositorios $repositorios)
    {
        $this->repositorios[] = $repositorios;

        return $this;
    }

    /**
     * Remove repositorios
     *
     * @param \ReservasBundle\Entity\Repositorios $repositorios
     */
    public function removeRepositorio(\ReservasBundle\Entity\Repositorios $repositorios)
    {
        $this->repositorios->removeElement($repositorios);
    }

    /**
     * Get repositorios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepositorios()
    {
        return $this->repositorios;
    }

    /**
     * Add proyectos
     *
     * @param \ReservasBundle\Entity\Proyectos $proyectos
     * @return Cabeceras
     */
    public function addProyecto(\ReservasBundle\Entity\Proyectos $proyectos)
    {
        $this->proyectos[] = $proyectos;

        return $this;
    }

    /**
     * Remove proyectos
     *
     * @param \ReservasBundle\Entity\Proyectos $proyectos
     */
    public function removeProyecto(\ReservasBundle\Entity\Proyectos $proyectos)
    {
        $this->proyectos->removeElement($proyectos);
    }

    /**
     * Get proyectos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProyectos()
    {
        return $this->proyectos;
    }

    public function getOcupada(){
        $numRepos = count($this->repositorios);
        $repos = 0;
        foreach ($this->repositorios as $key => $repositorio) {
            if($repositorio->getOcupado())
                $repos++;
        }
        if($repos == 0){
            return 'verde';
        } elseif ($repos < $numRepos){
            return 'naranja';
        } else {
            return 'rojo';
        }
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Cabeceras
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
