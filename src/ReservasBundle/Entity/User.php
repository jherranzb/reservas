<?php
// src/AppBundle/Entity/User.php

namespace ReservasBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    protected $token;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    public function getToken() {
        return $this->token;
    }
    
    public function setToken($token) {
        $this->token = $token;
        
        return $this;
    }
}
