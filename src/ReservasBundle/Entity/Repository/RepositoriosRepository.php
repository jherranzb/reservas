<?php

namespace ReservasBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * RepositoriosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RepositoriosRepository extends EntityRepository
{
}
