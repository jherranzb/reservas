<?php

namespace ReservasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ReservasType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fechainicio', 'text', array(
                    'attr' => array('class' => 'fieldset__input js__datepicker picker__input')
                    ))
                ->add('fechafin', 'text', array(
                    'attr' => array('class' => 'fieldset__input js__datepicker picker__input')
                    ))
                ->add('responsable')
                ->add('descripcion')
                ->add('repositorio', EntityType::class, array(
                    'class' => 'ReservasBundle:Repositorios',
                    'query_builder' => function(EntityRepository $repository) {
                        $repositories = $repository
                                        ->createQueryBuilder('r')
                                        ->innerJoin('ReservasBundle:Cabeceras', 'c', 'WITH', 'c.id = r.cabecera')
                                        ->orderBy('c.nombre', 'ASC');
                        return $repositories;
                    },
                    'expanded' => true,
                    'multiple' => true
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ReservasBundle\Entity\Reservas'
        ));
    }

}
