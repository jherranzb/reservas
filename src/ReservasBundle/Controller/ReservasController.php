<?php

namespace ReservasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ReservasBundle\Entity\Reservas;
use ReservasBundle\Form\ReservasType;
use ReservasBundle\Form\ReservasEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Reservas controller.
 *
 */
class ReservasController extends Controller {

    /**
     * @Route("/reservas", name="reservas_index")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $arrayCabeceras = array();
        $array = array();

        $cabeceras = $em->getRepository('ReservasBundle:Cabeceras')->findAll();

        foreach ($cabeceras as $indice => $cabecera) {
            $repositorios = $cabecera->getRepositorios();
            foreach ($repositorios as $key => $repositorio) {
                $posicion = $repositorio->getId() - 1;
                $datos = array();
                $datos['id'] = $posicion;
                $datos['cabecera'] = $cabecera->getnombre();
                $datos['repo'] = $repositorio->getNombre();
                array_push($arrayCabeceras, $datos);
            }
        }

        $reservas = $em->getRepository('ReservasBundle:Reservas')->findNextReservas();

        foreach ($reservas as $indice => $reserva) {
            $array[$indice] = array();
            $repo = $reserva->getRepositorio();
            $nombreCompleto = $repo->getNombreCompleto();
            $descripcion = $reserva->getDescripcion();
            $responsable = $reserva->getResponsable();
            $array[$indice]['title'] = "$responsable - $descripcion";
            $array[$indice]['start'] = $reserva->getFechaInicio()->format('Y-m-d H:i:s');
            $array[$indice]['end'] = $reserva->getFechaFin()->format('Y-m-d H:i:s');
            $array[$indice]['responsable'] = $reserva->getResponsable();
            $array[$indice]['descripcion'] = $descripcion;
            $array[$indice]['color'] = $reserva->getRepositorio()->getCabecera()->getColor();
            $array[$indice]['id'] = $reserva->getId();
            $array[$indice]['resourceId'] = $reserva->getRepositorio()->getId() - 1;
            $array[$indice]['fechaInicio'] = $reserva->getFechaInicio()->format('Y-m-d');
            $array[$indice]['fechaFin'] = $reserva->getFechaFin()->format('Y-m-d');
        }

        return $this->render('reservas/index.html.twig', array(
                    'reservas' => $reservas,
                    'array' => $array,
                    'arrayCabeceras' => $arrayCabeceras,
        ));
    }

    /**
     * @Route("/reservas/new", name="reservas_new")
     */
    public function newAction(Request $request) {
        $reserva = new Reservas();
        $form = $this->createForm('ReservasBundle\Form\ReservasType', $reserva);
        $form->handleRequest($request);

        //ldd($reserva);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserva);
            $em->flush();

            return $this->redirectToRoute('reservas_index');
        }

        return $this->render('reservas/new.html.twig', array(
                    'reserva' => $reserva,
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/show", name="reservas_show")
     */
    public function showAction(Reservas $reserva) {
        $deleteForm = $this->createDeleteForm($reserva);

        return $this->render('reservas/show.html.twig', array(
                    'reserva' => $reserva,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="reservas_edit")
     */
    public function editAction(Request $request, Reservas $reserva) {
        $deleteForm = $this->createDeleteForm($reserva);
        $editForm = $this->createForm('ReservasBundle\Form\ReservasEditType', $reserva);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserva);
            $em->flush();
            return $this->redirectToRoute('reservas_index');
        }

        return $this->render('reservas/edit.html.twig', array(
                    'reserva' => $reserva,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Reservas entity.
     *
     */
    public function deleteAction(Request $request, Reservas $reserva) {
        $form = $this->createDeleteForm($reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reserva);
            $em->flush();
        }

        return $this->redirectToRoute('reservas_index');
    }

    /**
     * Creates a form to delete a Reservas entity.
     *
     * @param Reservas $reserva The Reservas entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reservas $reserva) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('reservas_delete', array('id' => $reserva->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
