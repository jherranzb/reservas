<?php

namespace ReservasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ReservasBundle\Entity\Cabeceras;
use ReservasBundle\Form\CabecerasType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Cabeceras controller.
 *
 */
class AjaxController extends Controller {

    public function terminarReservaAction() {
        $params = $this->getRequest()->request->all()['id'];
        $em = $this->getDoctrine()->getManager();
        $reserva = $em->getRepository('ReservasBundle:Reservas')->find($params);
        $reserva->setFechaFin(date('Y-m-d H:i:s'));
        $em->persist($reserva);
        $em->flush();
        $response = array("code" => 100, "success" => true);
        //you can return result as JSON
        return new Response(json_encode($response));
    }

    public function saveReservaAction(Request $request) {
        $params = $this->getRequest()->request->all()['data'];
        $params = array_reverse($params);
        return new Response(json_encode($this->getResponse($params)));
    }

    public function getResponse($data) {
        $response = array("code" => 100, "success" => true);
        $em = $this->getDoctrine()->getManager();
        $reservas = $this->getEntities($data);
        foreach ($reservas as $reserva) {
            $em->persist($reserva);
            $em->flush();
        }
        return $response;
    }

    public function getEntities($data) {
        $entities = array();
        $dataLength = count($data);
        $repos = array_slice($data, 5);
        $data = array_chunk($data, 5)[0];
        return $this->createRepos($data, $repos);
    }

    public function createRepos($data, $repos) {
        $output = array();
        foreach ($repos as $key => $repo) {
            $output[$key] = $this->createRepo($data, $repo);
        }
        return $output;
    }

    public function createRepo($data, $repo) {
        $em = $this->getDoctrine()->getManager();
        $repositorio = $em->getRepository('ReservasBundle:Repositorios')->find($repo['value']);
        $reserva = new \ReservasBundle\Entity\Reservas();
        $reserva->setFechainicio(new \Datetime($data[4]['value']));
        $reserva->setFechafin(new \Datetime($data[3]['value']));
        $reserva->setResponsable($data[2]['value']);
        $reserva->setDescripcion($data[1]['value']);
        $reserva->setRepositorio($repositorio);
        return $reserva;
    }

}
