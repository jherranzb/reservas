<?php

namespace ReservasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ReservasBundle\Entity\Repositorios;
use ReservasBundle\Form\RepositoriosType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Repositorios controller.
 *
 */
class RepositoriosController extends Controller {

    /**
     * @Route("/repositorios", name="repositorios_index")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $repositorios = $em->getRepository('ReservasBundle:Repositorios')->findAll();

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $usuario = $this->getUser();
            $tokenUsuario = $usuario->getToken();
            if ($tokenUsuario != '') {
                $lastOpsRelease = array();
                $lastRelease = array();
                $lastTag = array();
                foreach ($repositorios as $indice => $repositorio) {
                    $repoId = $repositorio->getIdRepo($repositorio);
                    if ($repoId == null) {
                        $repositorio = $this->updateRepoId($repositorio);
                    }
                    $repoId = $repositorio->getIdRepo();
                    if ($repoId != null) {
                        $api = $this->get('gitlab_api');
                        $api->authenticate($tokenUsuario);

                        $branches = $api->api('repositories')->branches($repoId);
                        $tags = $api->api('repositories')->tags($repoId);

                        $lastOpsRelease[$indice] = $this->getLastOpsRelease($branches);
                        $lastRelease[$indice] = $this->getLastRelease($branches);
                        $lastTag[$indice] = $this->getLastTag($tags);
                    }
                }
                if ($repoId != null) {
                    return $this->render('repositorios/index.html.twig', array(
                                'repositorios' => $repositorios,
                                'lastTags' => $lastTag,
                                'lastOpsReleases' => $lastOpsRelease,
                                'lastRelease' => $lastRelease,
                    ));
                }
            }
        }

        return $this->render('repositorios/index.html.twig', array(
                    'repositorios' => $repositorios
        ));
    }

    /**
     * @Route("/repositorios/new", name="repositorios_new")
     */
    public function newAction(Request $request) {
        $repositorio = new Repositorios();
        $form = $this->createForm('ReservasBundle\Form\RepositoriosType', $repositorio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $securityContext = $this->container->get('security.authorization_checker');

            if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $usuario = $this->getUser();
                $tokenUsuario = $usuario->getToken();
                if ($tokenUsuario != '') {
                    $this->updateRepoId($repositorio);
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($repositorio);
                $em->flush();
            }
            return $this->redirectToRoute('repositorios_show', array('id' => $repositorio->getId()));
        }

        return $this->render('repositorios/new.html.twig', array(
                    'repositorio' => $repositorio,
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/show", name="repositorios_show")
     */
    public function showAction(Repositorios $repositorio) {
        $deleteForm = $this->createDeleteForm($repositorio);
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $usuario = $this->getUser();
            $tokenUsuario = $usuario->getToken();
            $id = -1;
            if ($tokenUsuario != '') {
                $repoId = $repositorio->getIdRepo($repositorio);
                if ($repoId == null) {
                    $repositorio = $this->updateRepoId($repositorio);
                }
                $repoId = $repositorio->getIdRepo();
                if ($repoId != null) {
                    $api = $this->get('gitlab_api');
                    $api->authenticate($tokenUsuario);
                    $project = $api->api('projects')->show($repoId);

                    $branches = $api->api('repositories')->branches($repoId);
                    $tags = $api->api('repositories')->tags($repoId);
                    $commits = $api->api('repositories')->commits($repoId, 0, 60);

                    $allBranches = $this->getBranches($branches);
                    $lastOpsRelease = $this->getLastOpsRelease($branches);
                    $lastRelease = $this->getLastRelease($branches);
                    $lastTag = $this->getLastTag($tags);

                    return $this->render('repositorios/show.html.twig', array(
                                'repositorio' => $repositorio,
                                'delete_form' => $deleteForm->createView(),
                                'branches' => $allBranches,
                                'lastOpsRelease' => $lastOpsRelease,
                                'lastRelease' => $lastRelease,
                                'lastTag' => $lastTag,
                                'commits' => $commits
                    ));
                }
            }
            ld(666);
        }
        return $this->render('repositorios/show.html.twig', array(
                    'repositorio' => $repositorio,
                    'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * @Route("/{id}/edit", name="repositorios_edit")
     */
    public function editAction(Request $request, Repositorios $repositorio) {
        $deleteForm = $this->createDeleteForm($repositorio);
        $editForm = $this->createForm('ReservasBundle\Form\RepositoriosType', $repositorio);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($repositorio);
            $em->flush();

            return $this->redirectToRoute('repositorios_index');
        }

        return $this->render('repositorios/edit.html.twig', array(
                    'repositorio' => $repositorio,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}/remove", name="repositorios_delete")
     */
    public function deleteAction(Request $request, Repositorios $repositorio) {
        $form = $this->createDeleteForm($repositorio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($repositorio);
            $em->flush();
        }

        return $this->redirectToRoute('repositorios_index');
    }

    /**
     * Creates a form to delete a Repositorios entity.
     *
     * @param Repositorios $repositorio The Repositorios entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Repositorios $repositorio) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('repositorios_delete', array('id' => $repositorio->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function updateRepoId($repositorio) {
        $securityContext = $this->container->get('security.authorization_checker');
        $usuario = $this->getUser();
        $tokenUsuario = $usuario->getToken();
        $api = $this->get('gitlab_api');
        $api->authenticate($tokenUsuario);
        $repoName = strtolower($repositorio->getNombre());
        $encontrado = false;

        for ($i = 1; $api->api('projects')->search($repoName, $i) != null || !$encontrado; $i++) {
            $gitProjects = $api->api('projects')->search($repoName, $i);
            $repoFullName = strtolower($repositorio->getNombreCompleto());

            foreach ($gitProjects as $indice => $project) {
                $name = strtolower($project['path_with_namespace']);
                if ($encontrado = ($repoFullName == $name)) {
                    $id = $project['id'];
                    $repositorio->setIdRepo($id);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($repositorio);
                    $em->flush();
                }
            }
        }
        return $repositorio;
    }

    public function getBranches($branches) {
        $branchesName = array();
        if ($branches == null) {
            return null;
        }
        foreach ($branches as $indice => $branch) {
            $branchName = $branch['name'];
            $branchesName[$indice] = $branchName;
        }
        return $branchesName;
    }

    public function getLastOpsRelease($branches) {
        $lastRelease = array();
        if ($branches == null) {
            return null;
        }
        foreach ($branches as $branch) {
            $branchName = $branch['name'];
            if (strpos($branchName, 'release') !== false) {
                $info = explode('/', $branchName);
                $fullVersion = ltrim($info[1], 'v');
                $version = implode('', explode('.', $fullVersion));
                if (empty($lastRelease) || intval($version) > $lastRelease['version']) {
                    $branchDate = $branch['commit']['committed_date'];
                    $lastRelease['version'] = intval($version);
                    $lastRelease['name'] = $branchName;
                    $lastRelease['date'] = str_replace('T', ' ', $branchDate);
                }
            }
        }
        return $lastRelease;
    }

    public function getLastRelease($branches) {
        $lastRelease = array();
        if ($branches == null) {
            return null;
        }
        foreach ($branches as $branch) {
            $branchName = $branch['name'];
            if (strpos($branchName, 'release') !== false) {
                $date = $branch['commit']['committed_date'];
//                $actualDate = $lastRelease['date'];
                if (empty($lastRelease) || strtotime($date) > strtotime($lastRelease['date'])) {
                    $branchDate = $branch['commit']['committed_date'];
                    $lastRelease['name'] = $branchName;
                    $lastRelease['date'] = str_replace('T', ' ', $branchDate);
                }
            }
        }
        return $lastRelease;
    }

    public function getLastTag($tags) {
        $lastTag = array();
        $lastTag['version'] = "0.0";
        if ($tags == null) {
            return null;
        }
        foreach ($tags as $tag) {
            $tagVersion = $tag['name'];
            $version = ltrim($tagVersion, 'v');
            $actualVersion = $lastTag['version'];
            if (empty($lastTag) || version_compare($actualVersion, $version) == -1) {
                $tagDate = $tag['commit']['committed_date'];
                $actualVersion = $tagVersion;
                $lastTag['version'] = $version;
                $lastTag['name'] = $tagVersion;
                $lastTag['date'] = str_replace('T', ' ', $tagDate);
            }
        }
        return $lastTag;
    }

}
