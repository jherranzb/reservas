<?php

namespace ReservasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ReservasBundle\Entity\Cabeceras;
use ReservasBundle\Form\CabecerasType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Cabeceras controller.
 *
 */
class CabecerasController extends Controller
{
    /**
     * @Route("/cabeceras", name="cabeceras_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cabeceras = $em->getRepository('ReservasBundle:Cabeceras')->findAll();

        return $this->render('cabeceras/index.html.twig', array(
            'cabeceras' => $cabeceras,
        ));
    }

    /**
     * @Route("/cabeceras/new", name="cabeceras_new")
     */
    public function newAction(Request $request)
    {
        $cabecera = new Cabeceras();
        $form = $this->createForm('ReservasBundle\Form\CabecerasType', $cabecera);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cabecera);
            $em->flush();

            return $this->redirectToRoute('cabeceras_index');
        }

        return $this->render('cabeceras/new.html.twig', array(
            'cabecera' => $cabecera,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/show", name="cabeceras_show")
     */
    public function showAction(Cabeceras $cabecera)
    {
        $deleteForm = $this->createDeleteForm($cabecera);

        return $this->render('cabeceras/show.html.twig', array(
            'cabecera' => $cabecera,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/cabeceras/{id}/edit", name="cabeceras_edit")
     */
    public function editAction(Request $request, Cabeceras $cabecera)
    {
        $deleteForm = $this->createDeleteForm($cabecera);
        $editForm = $this->createForm('ReservasBundle\Form\CabecerasType', $cabecera);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cabecera);
            $em->flush();

            return $this->redirectToRoute('cabeceras_edit', array('id' => $cabecera->getId()));
        }

        return $this->render('cabeceras/edit.html.twig', array(
            'cabecera' => $cabecera,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cabeceras entity.
     * @Route("/cabeceras/{id}/remove", name="cabeceras_delete")
     *
     */
    public function deleteAction(Request $request, Cabeceras $cabecera)
    {
        $form = $this->createDeleteForm($cabecera);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cabecera);
            $em->flush();
        }

        return $this->redirectToRoute('cabeceras_index');
    }

    /**
     * Creates a form to delete a Cabeceras entity.
     *
     * @param Cabeceras $cabecera The Cabeceras entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cabeceras $cabecera)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cabeceras_delete', array('id' => $cabecera->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
