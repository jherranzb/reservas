<?php

namespace ReservasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $cabeceras = $em->getRepository('ReservasBundle:Cabeceras')->findAll();

        $reservas = $em->getRepository('ReservasBundle:Reservas')->getTodayReservas();

        $arrayReservasToday = array();

        foreach ($reservas as $indice => $reserva) {
            $arrayReservasToday[$indice] = array();
            $repo = $reserva->getRepositorio();
            $nombreCompleto = $repo->getNombreCompleto();
            $descripcion = $reserva->getDescripcion();
            $responsable = $reserva->getResponsable();
            $arrayReservasToday[$indice]['title'] = "$responsable - $descripcion";
            $arrayReservasToday[$indice]['start'] = $reserva->getFechaInicio()->format('Y-m-d H:i:s');
            $arrayReservasToday[$indice]['end'] = $reserva->getFechaFin()->format('Y-m-d H:i:s');
            $arrayReservasToday[$indice]['responsable'] = $reserva->getResponsable();
            $arrayReservasToday[$indice]['descripcion'] = $descripcion;
            $arrayReservasToday[$indice]['color'] = $reserva->getRepositorio()->getCabecera()->getColor();
            $arrayReservasToday[$indice]['id'] = $reserva->getId();
            $arrayReservasToday[$indice]['resourceId'] = $reserva->getRepositorio()->getId() - 1;
            $arrayReservasToday[$indice]['fechaInicio'] = $reserva->getFechaInicio()->format('Y-m-d');
            $arrayReservasToday[$indice]['fechaFin'] = $reserva->getFechaFin()->format('Y-m-d');
        }

        $arrayCabeceras = array();

        $cabeceras = $em->getRepository('ReservasBundle:Cabeceras')->findAll();

        foreach ($cabeceras as $indice => $cabecera) {
            $repositorios = $cabecera->getRepositorios();
            foreach ($repositorios as $key => $repositorio) {
                $posicion = $repositorio->getId() - 1;
                $datos = array();
                $datos['id'] = $posicion;
                $datos['cabecera'] = $cabecera->getnombre();
                $datos['repo'] = $repositorio->getNombre();
                array_push($arrayCabeceras, $datos);
            }
        }


        return $this->render('ReservasBundle:Default:index.html.twig', array(
                    'cabeceras' => $cabeceras,
                    'arrayCabeceras' => $arrayCabeceras,
                    'reservas' => $arrayReservasToday
        ));
    }

}
