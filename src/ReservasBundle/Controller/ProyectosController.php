<?php

namespace ReservasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ReservasBundle\Entity\Proyectos;
use ReservasBundle\Form\ProyectosType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Proyectos controller.
 *
 */
class ProyectosController extends Controller
{
    /**
     * @Route("/proyectos", name="proyectos_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $proyectos = $em->getRepository('ReservasBundle:Proyectos')->findAll();
        
        $proyectos = array_reverse($proyectos);

        return $this->render('proyectos/index.html.twig', array(
            'proyectos' => $proyectos,
        ));
    }

    /**
     * @Route("/proyectos/new", name="proyectos_new")
     */
    public function newAction(Request $request)
    {
        $proyecto = new Proyectos();
        $form = $this->createForm('ReservasBundle\Form\ProyectosType', $proyecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($proyecto);
            $em->flush();

            return $this->redirectToRoute('proyectos_show', array('id' => $proyecto->getId()));
        }

        return $this->render('proyectos/new.html.twig', array(
            'proyecto' => $proyecto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/show", name="proyectos_show")
     */
    public function showAction(Proyectos $proyecto)
    {
        $deleteForm = $this->createDeleteForm($proyecto);

        return $this->render('proyectos/show.html.twig', array(
            'proyecto' => $proyecto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="proyectos_edit")
     */
    public function editAction(Request $request, Proyectos $proyecto)
    {
        $deleteForm = $this->createDeleteForm($proyecto);
        $editForm = $this->createForm('ReservasBundle\Form\ProyectosType', $proyecto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($proyecto);
            $em->flush();

            return $this->redirectToRoute('proyectos_edit', array('id' => $proyecto->getId()));
        }

        return $this->render('proyectos/edit.html.twig', array(
            'proyecto' => $proyecto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Proyectos entity.
     *
     */
    public function deleteAction(Request $request, Proyectos $proyecto)
    {
        $form = $this->createDeleteForm($proyecto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($proyecto);
            $em->flush();
        }

        return $this->redirectToRoute('proyectos_index');
    }

    /**
     * Creates a form to delete a Proyectos entity.
     *
     * @param Proyectos $proyecto The Proyectos entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Proyectos $proyecto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('proyectos_delete', array('id' => $proyecto->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
